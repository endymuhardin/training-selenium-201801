package com.muhardin.endy.belajar.selenium;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KalkulatorTests {
    
    @After
    public void cleanupTest(){
        System.out.println("Setelah test selesai, bersihkan sisa2nya");
    }
    
    @AfterClass
    public static void cleanupGlobal(){
        System.out.println("Menjalankan clean up global");
    }
    
    @Before
    public void inisialisasiTest(){
        System.out.println("Biasanya disini reset data sebelum masing-masing test");
    }
    
    @Test
    public void testTambah(){
        Kalkulator k = new Kalkulator();
        int hasil = k.tambah(2,3);
        System.out.println("2 + 3 = "+hasil);
        
        // suruh JUnit yang periksa hasilnya
        Assert.assertEquals("2 + 3 harusnya 5", 5, k.tambah(2,3));
    }
    
    @Test
    public void testKurang(){
        Kalkulator k = new Kalkulator();
        int hasil = k.kurang(2,3);
        System.out.println("2 - 3 = "+hasil);
        
        // suruh JUnit yang periksa hasilnya
        Assert.assertEquals(-1, k.kurang(2,3));
    }
    
    @BeforeClass
    public static void inisialisasiGlobal(){
        System.out.println("Menjalankan inisialisasi global");
    }
    
}