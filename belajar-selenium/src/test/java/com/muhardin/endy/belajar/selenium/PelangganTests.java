package com.muhardin.endy.belajar.selenium;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.muhardin.endy.belajar.selenium.helper.SeleniumUtilities;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PelangganTests {
    
    private static final String URL_APLIKASI = "http://training-selenium.herokuapp.com/pelanggan";
    private static final String PESAN_SUKSES = "Data pelanggan berhasil disimpan";
    private static final String PESAN_GAGAL_HARUS_DIISI = "must not be empty";
    private static final String PESAN_GAGAL_FORMAT_EMAIL = "must be a well-formed email address";
    
    private static final String XPATH_TOMBOL_SIMPAN = "/html/body/form/table/tbody/tr[3]/td[2]/input";
    private static final String XPATH_PESAN_ERROR_NAMA = "/html/body/form/table/tbody/tr[1]/td[3]/ul/li";
    private static final String XPATH_PESAN_ERROR_EMAIL = "/html/body/form/table/tbody/tr[2]/td[3]/ul/li";
    
    private static WebDriver webDriver;
    private Faker faker = new Faker();
    
    @BeforeClass
    public static void jalankanBrowser(){
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testSimpanNormal() {
        Name name = faker.name();
        String nama = name.fullName();
        String email = name.username()+"@"+faker.internet().domainName();
        
        webDriver.get(URL_APLIKASI+"/list");
        
        WebElement linkTambah = webDriver.findElement(By.linkText("Tambah Data Pelanggan"));
        linkTambah.click();
        
        Boolean pindahKeHalamanForm = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/form"));
        
        Assert.assertTrue(pindahKeHalamanForm);
        
        WebElement inputNama = webDriver.findElement(By.name("nama"));
        WebElement inputEmail = webDriver.findElement(By.name("email"));
        
        inputNama.sendKeys(nama);
        inputEmail.sendKeys(email);
        inputNama.submit();
        
        Boolean pindahKeHalamanList = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/list"));
        
        Assert.assertTrue(pindahKeHalamanList);
        Assert.assertTrue(webDriver.findElement(By.tagName("body"))
                .getText().contains(PESAN_SUKSES));
        SeleniumUtilities.ambilScreenshot(webDriver, "simpan-normal");
    }
    
    @Test
    public void testNamaKosong(){
        webDriver.get(URL_APLIKASI+"/form");
        
        WebElement inputEmail = webDriver.findElement(By.name("email"));
        WebElement tombolSimpan = webDriver.findElement(By.xpath(XPATH_TOMBOL_SIMPAN));
        
        inputEmail.sendKeys(faker.name().username()+"@"+faker.internet().domainName());
        tombolSimpan.click();
        
        Boolean tetapDiHalamanForm = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/form"));
        
        Assert.assertTrue(tetapDiHalamanForm);
        
        WebElement pesanError = webDriver.findElement(By.xpath(XPATH_PESAN_ERROR_NAMA));
        Assert.assertNotNull(pesanError);
        Assert.assertEquals(PESAN_GAGAL_HARUS_DIISI, pesanError.getText());
        
        SeleniumUtilities.ambilScreenshot(webDriver, "validasi-nama-kosong");
    }
    
    @Test
    public void testEmailKosong(){
        webDriver.get(URL_APLIKASI+"/form");
        
        WebElement inputNama = webDriver.findElement(By.name("nama"));
        WebElement tombolSimpan = webDriver.findElement(By.xpath(XPATH_TOMBOL_SIMPAN));
        
        inputNama.sendKeys(faker.name().fullName());
        tombolSimpan.click();
        
        Boolean tetapDiHalamanForm = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/form"));
        
        Assert.assertTrue(tetapDiHalamanForm);
        
        WebElement pesanError = webDriver.findElement(By.xpath(XPATH_PESAN_ERROR_EMAIL));
        Assert.assertNotNull(pesanError);
        Assert.assertEquals(PESAN_GAGAL_HARUS_DIISI, pesanError.getText());
        
        SeleniumUtilities.ambilScreenshot(webDriver, "validasi-email-kosong");
    }
    
    @Test
    public void testFormatEmailSalah(){
        webDriver.get(URL_APLIKASI+"/form");
        
        WebElement inputNama = webDriver.findElement(By.name("nama"));
        WebElement inputEmail = webDriver.findElement(By.name("email"));
        WebElement tombolSimpan = webDriver.findElement(By.xpath(XPATH_TOMBOL_SIMPAN));
        
        inputNama.sendKeys(faker.name().fullName());
        inputEmail.sendKeys(faker.name().username());
        
        tombolSimpan.click();
        
        Boolean tetapDiHalamanForm = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/form"));
        
        Assert.assertTrue(tetapDiHalamanForm);
        
        WebElement pesanError = webDriver.findElement(By.xpath(XPATH_PESAN_ERROR_EMAIL));
        Assert.assertNotNull(pesanError);
        Assert.assertEquals(PESAN_GAGAL_FORMAT_EMAIL, pesanError.getText());
        
        SeleniumUtilities.ambilScreenshot(webDriver, "validasi-format-email");
    }
}
