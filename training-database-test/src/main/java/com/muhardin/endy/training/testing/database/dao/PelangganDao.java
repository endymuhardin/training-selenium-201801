package com.muhardin.endy.training.testing.database.dao;

import com.muhardin.endy.training.testing.database.entity.Pelanggan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PelangganDao extends PagingAndSortingRepository<Pelanggan, String>{
    public Pelanggan findByEmail(String email);
}
